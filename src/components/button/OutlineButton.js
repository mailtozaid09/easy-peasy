import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, useColorScheme, ActivityIndicator, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';



const OutlineButton = ({title, onPress, disabled, buttonLoader }) => {

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.5}
            style={[styles.buttonContainer]}
            disabled={buttonLoader ? true : disabled}
        >
            {buttonLoader 
            ?
            <ActivityIndicator size="large"  color={colors.primary} />
            :
            <Text style={[styles.buttonText]} >{title}</Text>
            }
        </TouchableOpacity>
    )
}



const styles = StyleSheet.create({
    buttonContainer: {
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        borderWidth: 1,
        borderColor: colors.primary,
        backgroundColor: colors.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 4,
    },
    buttonText: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.primary,
    }
})

export default OutlineButton
