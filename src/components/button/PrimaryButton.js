import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, useColorScheme, ActivityIndicator, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';



const PrimaryButton = ({title, onPress, disabled, buttonLoader }) => {

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.5}
            style={[styles.buttonContainer, {backgroundColor: disabled ? colors.bg_color : colors.primary}]}
            disabled={buttonLoader ? true : disabled}
        >
            {buttonLoader 
            ?
            <ActivityIndicator size="large"  color={colors.white} />
            :
            <Text style={[styles.buttonText, {color: disabled ? colors.dark_gray : colors.white}]} >{title}</Text>
            }
        </TouchableOpacity>
    )
}



const styles = StyleSheet.create({
    buttonContainer: {
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        marginTop: 15,
        marginBottom: 15,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        backgroundColor: colors.light_gray,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 8,
    },
    buttonText: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    }
})

export default PrimaryButton
