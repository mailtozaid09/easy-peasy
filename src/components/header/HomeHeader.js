import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { useNavigation } from '@react-navigation/native';

import Icon from '../../utils/icons';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { screenWidth } from '../../global/constants';

const HomeHeader = ({userDetails}) => {

    const navigation = useNavigation()
    
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <TouchableOpacity 
                    activeOpacity={0.5} 
                    onPress={() => {navigation.navigate('Profile')}}
                    style={{height: 40, width: 40, borderRadius: 20, justifyContent: 'center', backgroundColor: colors.primary, alignItems: 'center'}} >
                    <Icon type={'FontAwesome'} name={'user'}  size={26} color={colors.white} />
                </TouchableOpacity>
                <View style={{flexDirection: 'row', alignItems: 'center',}} >
                    <TouchableOpacity 
                        activeOpacity={0.5} 
                        onPress={() => {navigation.navigate('Notification')}}
                        >
                        <Icon type={'MaterialCommunityIcons'} name={'bell-badge-outline'}  size={30} color={colors.primary}  />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles =  StyleSheet.create({
    container: {
        height: 56,
        width: screenWidth,
        borderBottomWidth: 0.5,
        paddingHorizontal: 20,
        justifyContent: 'center',
        backgroundColor: colors.white,
        borderColor: colors.light_gray,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subtitle: {
        fontSize: 18,
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
})

export default HomeHeader