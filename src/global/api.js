
const production_url = 'https://bikehub-s8of.onrender.com/api/v1'

//const localhost_url = 'http://10.201.101.204:8080/api/v1'
//const localhost_url = 'http://172.20.10.3:8080/api/v1'
// const localhost_url = 'http://192.168.0.136:8080/api/v1'
const localhost_url = 'http://192.168.1.6:8080/api/v1'

const baseUrl = production_url


// Products
export function getAllProducts() {
    return(
        fetch(
            `${baseUrl}/products`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

export function addNewProductDetails(formData) {
    return(
        fetch(
            `${baseUrl}/products`,
            {
                method: 'POST',
                headers: {
                    'Accept': "application/json",
                    'Content-Type': 'multipart/form-data',
                },
                body: formData
            }
        )
        .then(res => res.json())
    );
}



// Order

// Get All Orders 
export function getAllOrders() {
    return(
        fetch(
            `${baseUrl}/orders`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

// Get Orders By User Id
export function getOrdersByUserId(id) {
    return(
        fetch(
            `${baseUrl}/orders/get/userorders/${id}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

// Add New Order 

export function addNewOrderDetails(body) {
    return(
        fetch(
            `${baseUrl}/orders`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}




// Categories
export function getAllCategories() {
    return(
        fetch(
            `${baseUrl}/categories`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}



// Shipping Address

export function getAllShippingAddress() {
    return(
        fetch(
            `${baseUrl}/address`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

export function addNewShippingAddress(body) {
    return(
        fetch(
            `${baseUrl}/address`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

export function deleteShippingAddress(id) {
    return(
        fetch(
            `${baseUrl}/address/${id}`,
            {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}



// User
export function createNewUser(body) {
    return(
        fetch(
            `${baseUrl}/users`,
            {
                method: 'POST',
                headers: {
                    'Accept': "application/json",
                    'Content-Type': 'multipart/form-data',
                },
                body: body
            }
        )
        .then(res => res.json())
    );
}

export function updateUserDetails(body, id) {
    return(
        fetch(
            `${baseUrl}/users/${id}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}



// Login
export function checkUserExist(body, type) {
    return(
        fetch(
            `${baseUrl}/checkUser/${type}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

export function userLogin(body) {
    return(
        fetch(
            `${baseUrl}/users/login`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

// Send Email OTP
export function sendEmailOtp(body) {
    return(
        fetch(
            `${baseUrl}/getotp`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

// Verify Email OTP
export function verifyEmailOtp(body) {
    return(
        fetch(
            `${baseUrl}/verifyOtp`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}


// Send Phone OTP
export function sendPhoneOtp(body) {
    return(
        fetch(
            `${baseUrl}/login/sendPhoneOtp`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}

// Verify Phone OTP
export function verifyPhoneOtp(body) {
    return(
        fetch(
            `${baseUrl}/login/verifyPhoneOtp`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}