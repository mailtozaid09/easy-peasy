import { colors } from "./colors";
import { media } from "./media";

export const sampleObj = [
    {
        id: 1,
        time: '00:00',
        title: 'Maldives',
        subtitle: 'Save the Turtles',
        icon: media.image1,
    },
    {
        id: 2,
        time: '04:00',
        title: 'Golden beach',
        subtitle: 'Surfing on the sea',
        icon: media.image2,
    },
    {
        id: 3,
        time: '08:00',
        title: 'Coconut grove',
        subtitle: 'BBQ party by the sea',
        icon: media.image3,
    },
    {
        id: 4,
        time: '12:00',
        title: 'Maldives Islands',
        subtitle: 'Sea blowing',
        icon: media.image4,
    },
    {
        id:54,
        time: '16:00',
        title: 'Maldives',
        subtitle: 'Save the Turtles',
        icon: media.image1,
    },
    {
        id: 6,
        time: '20:00',
        title: 'Golden beach',
        subtitle: 'Surfing on the sea',
        icon: media.image2,
    },
]