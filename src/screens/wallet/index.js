import React, { useState, useEffect } from 'react';
import {Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, SafeAreaView, } from 'react-native';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';


const WalletScreen = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Text style={styles.title} >Wallet</Text>
        </SafeAreaView>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: colors.off_white,
    },
    title: {
        fontSize: 36,
        fontFamily: Poppins.Regular,
        color: colors.dark_gray,
    },
})

export default WalletScreen