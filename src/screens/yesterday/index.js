import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, } from 'react-native';
import { screenWidth } from '../../global/constants';
import { Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';
import { media } from '../../global/media';
import { sampleObj } from '../../global/sampleData';

import { useFocusEffect } from '@react-navigation/native';
import moment from 'moment';

const YesterdayScreen = () => {

    const [date, setDate] = useState(new Date());
    var new_date = moment(date).add(-1, 'days');
    const [time, setTime] = useState(new_date);
    
    useEffect(() => {
 
    }, [])
    
    useFocusEffect(
        React.useCallback(() => {
            const intervalId = setInterval(() => {
                console.log("Screen was focused");
                
                var new_date = moment(new Date()).add(-1, 'days');
                setTime(new_date);
            }, 60000);
    
            return () => {
                console.log("Screen was not focused");
                clearInterval(intervalId)
            };
        }, [])
    );

    const checkIfTimePassed = (val) => {
        var getTime = moment(time).format("DD/MM/YYY | HH:mm")

        var beginningTime = moment(val, 'DD/MM/YYY | HH:mm');
        var endTime = moment(getTime, 'DD/MM/YYY | HH:mm');

        return beginningTime.isBefore(endTime)
    }


    const checkIfTimeHourPassed = (val) => {
        var getTime = moment(time).format("DD/MM/YYY | HH:mm")

        var beginningTime = moment(val, 'DD/MM/YYY | HH:mm');
        var currentTime = moment(getTime, 'DD/MM/YYY | HH:mm');
        var nextSlotTime = moment(beginningTime).add(4, 'hours')
        var height = 50
       
        if (currentTime.isBetween(beginningTime, nextSlotTime)) {
            
            var duration = moment.duration(currentTime.diff(beginningTime));
            var hours = duration.hours();

            height = (50 * 25 * hours)/100

        } else {
            if(beginningTime.isBefore(currentTime)){
                 height = 50
            }else{
                height = 0
            }
        }
        return (new Date() > time) ? 50 : height
    }

    

    return (
        <View style={styles.container}>
            <View style={styles.mainContainer}>

                {sampleObj?.map((item, index) => (
                    <View key={index} style={{ flexDirection: 'row' }} >
                        <View style={{ flexDirection: 'row', }} >
                            <View style={{width: 55,}} >
                                <Text style={styles.time} >{item.time}</Text>
                            </View>
                            <View style={{ width: 30, marginRight: 10, alignItems: 'center', }} >
                                <View style={[styles.circleStyle, checkIfTimePassed(item.time) || (new Date() > time) ? {backgroundColor: colors.primary} : { borderWidth: 2, borderColor: colors.light_gray},  ]}>
                                    {checkIfTimePassed(item.time) || (new Date() > time) && <Image source={media.map} style={styles.circleIcon} />}
                                </View>
                                {index < sampleObj?.length - 1 && 
                                    <View style={[styles.barStyle, {backgroundColor: colors.light_gray} ]}>
                                        <View style={{height: checkIfTimeHourPassed(item.time), width: 2, backgroundColor: colors.primary}} />
                                    </View>
                                }
                            </View>
                        </View>
                        <View style={{flex: 1, }} >
                            <Text style={styles.title} >{item.title}</Text>   
                            <Text style={styles.subtitle} >{item.subtitle}</Text>    
                        </View>
                        <View style={styles.imageIconContainer} >
                            <Image source={item.icon} style={styles.imageIcon} />
                        </View>
                    </View>
                ))}
                
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center', 
        width: screenWidth,
    },
    mainContainer: {
        width: '100%',
        marginTop: 20, 
        padding: 20, 
    },
    circleStyle: {
        height: 26, 
        width: 26,
        borderRadius: 13,
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleIcon: {
        height: 18,
        width: 18,
        resizeMode: 'contain',
    },
    barStyle: {
        height: 50, 
        width: 2,
        alignItems: 'center',
    },
    time: {
        fontSize: 18,
        fontFamily: Poppins.Regular,
        color: colors.black,
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subtitle: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: colors.gray,
    },
    imageIcon: {
        height: 40,
        width: 40,
        resizeMode: 'contain',
    },
    imageIconContainer: {
        height: 44,
        width: 44,
        borderRadius: 22,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
    },
})

export default YesterdayScreen