import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import Input from '../../components/input'
import PrimaryButton from '../../components/button/PrimaryButton'
import Icon from '../../utils/icons'

import { useDispatch, useSelector } from 'react-redux'

const LoginScreen = (props) => {

    const navigation = props.navigation
    const dispatch = useDispatch()

    const [mobileNumber, setMobileNumber] = useState('');

    const [errors, setErrors] = useState({});
    const [apiError, setApiError] = useState(null);
    const [buttonLoader, setButtonLoader] = useState(false);

    useEffect(() => {
  
    }, [])
    
    const loginFunction = () => {
        setButtonLoader(true)
        var isValid = true
        if(mobileNumber.length != 10){
            console.log("Please enter a valid number");
            isValid = false
            setErrors((prev) => {
                return {...prev, number: 'Please enter a valid number!'}
            })
        }

        if(isValid){
            setButtonLoader(false)
            navigation.navigate('OTPVerification', {params: mobileNumber});
        }else{
            setButtonLoader(false)
        }
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView keyboardShouldPersistTaps="always" contentContainerStyle={{flex: 1}} >
                <View style={styles.mainContainer} >
                    <View style={{}} >
                        <View style={styles.iconContainer}>
                            <Icon type="MaterialCommunityIcons" name={'cellphone-text'} size={34} color={colors.black} />
                        </View>

                        <View style={{marginTop: 20, marginBottom: 10}} >
                            <Text style={styles.title} >Continue with phone</Text>  
                            <Text style={styles.subTitle} >Sign up with your phone number.</Text>            
                        </View>

                        <View>

                            <Input
                                label="Phone Number"
                                placeholder="Enter your phone number"
                                isNumber
                                error={errors.number}
                                value={mobileNumber}
                                onChangeText={(text) => {setMobileNumber(text); setErrors({}); }}
                            />
                        </View>
                    </View>
                
                   

                    <View>
                        <View style={{alignItems: 'center', width: '100%'}} >
                            {apiError && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{apiError}</Text>}
                        </View>
                        
                        <PrimaryButton
                            title="Continue" 
                            buttonLoader={buttonLoader}
                            onPress={() => {
                                loginFunction()
                            }}
                        />
                    </View>

                </View>
            </ScrollView>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        //alignItems: 'center',
        justifyContent: 'space-between',
    },
    iconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: colors.light_gray,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.gray_color,
        marginBottom: 10,
    },
})


export default LoginScreen