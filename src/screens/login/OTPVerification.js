import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, Button, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, Alert, ToastAndroid, } from 'react-native'


import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'


import Icon from '../../utils/icons'
import PrimaryButton from '../../components/button/PrimaryButton'
import AlertModal from '../../components/modal/AlertModal'

import auth from '@react-native-firebase/auth'
import OTPTextView from 'react-native-otp-textinput';

import { useDispatch } from 'react-redux'
import { setUserDetails, userLoggedIn } from '../../store/modules/auth/actions'
import { LocalNotification } from '../../utils/LocalPushController'


const picture_url = "https://images.unsplash.com/photo-1578894381163-e72c17f2d45f?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTA5fHx0cmF2ZWx8ZW58MHx8MHx8fDA%3D"

const OTPVerification = (props) => {

    const dispatch = useDispatch()
    const input = useRef(null);

    const [errors, setErrors] = useState({});
    const [otpValue, setOtpValue] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');

    const [confirm, setConfirm] = useState(null);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [buttonLoader, setButtonLoader] = useState(false);

    useEffect(() => {
        var number = props?.route?.params?.params
        var num = '+91 ' + props?.route?.params?.params

        setMobileNumber(num)
        signInWithPhoneFunc(number)

    }, [])


    const signInWithPhoneFunc = async (val) => {
        console.log("signInWithPhoneFunc >>",val);
        var num = '+91' + val
        try {
            const confirmation = await auth().signInWithPhoneNumber(num);
            console.log('====================================');
            console.log("confirmation-> ",confirmation);
            console.log('====================================');
            setConfirm(confirmation);
        } catch (error) {
            checkForErrors(error)
        }
    }

    const saveUserDetails = () => {

            dispatch(userLoggedIn(true))

            dispatch(setUserDetails({
                mobileNumber: mobileNumber
            }))

            
            
            LocalNotification({
                picture: picture_url,
                title: "You've successfully logged in!",
                message: `Enjoy your experience with Easy-Peasy Travel App.`,
            })

            setConfirm(null);
            setAlertType('Success')
            setAlertTile("Success!!!");
            setAlertDescription('User Logged In Successfully!');
            setShowAlertModal(true)
    }

    const checkForErrors = (error) => {
        setAlertType('Error')
        setAlertTile("Error!!!");
        setShowAlertModal(true)

        if (error.code === "auth/too-many-requests") {
            setAlertDescription("We have blocked all requests from this device due to unusual activity. Try again later!");
            console.log('All request blocked!', error);
        }else if (error.code === "auth/invalid-verification-code") {
            setAlertDescription("Invalid Verification Code!");
            console.log('Invalid Verification Code! => ', error);
        }else if (error.code === "auth/invalid-phone-number") {
            setAlertDescription("Invalid Phone Number!");
            console.log('Invalid Verification Code! => ', error);
        }else if (error.code === "auth/session-expired") {
            setAlertDescription("The sms code has expired. Please re-send the verification code to try again!");
            console.log('The sms code has expired. Please re-send the verification code to try again! => ', error);
        }else {
            setAlertDescription(error.code);
            console.log("Error =>> ", error);
        }
    }

    const phoneVerificationCode = async (code) => {
        try {
            await confirm.confirm(code);
            
            setButtonLoader(false)
            saveUserDetails()
        } catch (error) {
            setButtonLoader(false)
            checkForErrors(error)
        }
    }


    const verifyFunction = () => {
        setButtonLoader(true)
        var isValid = true
                
        if(otpValue.length != 6){
            console.log("Please enter a valid OTP");
            isValid = false
            setErrors((prev) => {
                return {...prev, OTP: 'Please enter a valid OTP'}
            })
        }

        if(isValid){
            phoneVerificationCode(otpValue)
        }else{
            setButtonLoader(false)
        }


    }
    
    return (
        <SafeAreaView style={styles.container} >           
           <ScrollView keyboardShouldPersistTaps="always" contentContainerStyle={{flex: 1}} >
                <View style={styles.mainContainer} >
                    <View style={{}} >
                        <View style={styles.iconContainer}>
                            <Icon type="MaterialCommunityIcons" name={'cellphone-text'} size={34} color={colors.black} />
                        </View>

                        <View style={{marginTop: 20, marginBottom: 10}} >
                            <Text style={styles.title} >Verify you mobile number</Text>
                            <Text style={styles.subTitle} >OTP has been sent on the regsterd phone number - <Text>{mobileNumber}</Text>.</Text>            
                        </View>

                        <View style={styles.otpContainer} >
                            <OTPTextView
                                ref={input}
                                containerStyle={styles.textInputContainer}
                                handleTextChange={(otp) => {setOtpValue(otp); setErrors({})}}
                                inputCount={6}
                                tintColor={colors.primary}
                                keyboardType="numeric"
                                textInputStyle={styles.textInputStyle}
                            />
                        
                            {errors.OTP && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{errors?.OTP}</Text>}
                        </View>
                    </View>
                
                   

                    <View>
                        <PrimaryButton
                            title="Verify OTP" 
                            buttonLoader={buttonLoader}
                            onPress={() => {
                                verifyFunction()
                            }}
                        />
                    </View>

                </View>
            </ScrollView>
            {showAlertModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onAdd={() => {setShowAlertModal(false); props.navigation.navigate('Tabbar')}}
                    closeModal={() => {setShowAlertModal(false); }} 
                />
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    textInputStyle: {
        height: 44,
        width: 44,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    iconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: colors.light_gray,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    subTitle: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.gray_color,
        marginBottom: 10,
    },
})

export default OTPVerification