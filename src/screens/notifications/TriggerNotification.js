import React, { useState, useEffect } from 'react';
import {Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, SafeAreaView, ScrollView, ToastAndroid, } from 'react-native';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import Input from '../../components/input';
import PrimaryButton from '../../components/button/PrimaryButton';


import DatePicker from 'react-native-date-picker'
import Icon from '../../utils/icons';
import moment from 'moment';
import { ScheduleLocalNotification } from '../../utils/LocalPushController';
import { screenWidth } from '../../global/constants';

const picture_url = "https://images.unsplash.com/photo-1578894381163-e72c17f2d45f?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTA5fHx0cmF2ZWx8ZW58MHx8MHx8fDA%3D"

const tagOptions = [
    {
        id: 1,
        title: 'Yesterday',
    },
    {
        id: 2,
        title: 'Today',
    },
    {
        id: 1,
        title: 'Tomorrow',
    },
]

const TriggerNotification = ({navigation}) => {

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})

    const [currentTag, setCurrentTag] = useState('Today');

    const [date, setDate] = useState(new Date())

    const [scheduleDate, setScheduleDate] = useState(date)
    const [showScheduleDate, setShowscheduleDate] = useState(false)

    const [buttonLoader, setButtonLoader] = useState(false);

    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
    }

    const triggerNotification = () => {
        
        var isValid = true

        setButtonLoader(true)
         
        if(!form.title){
            console.log("Please enter a valid title");
            isValid = false;
            setErrors((prev) => {
                return {...prev, title: 'Please enter a valid title!'}
            })
        }

        if(!form.subtitle){
            console.log("Please enter a valid subtitle");
            isValid = false;
            setErrors((prev) => {
                return {...prev, subtitle: 'Please enter a valid subtitle!'}
            })
        }
        

        var date = new Date()
        var firstDate = moment(date).format('HH:mm A')
        let secondDate =  moment(scheduleDate).format('HH:mm A')

        if(firstDate > secondDate){
            console.log("Please enter a valid date");
            isValid = false;
            setErrors((prev) => {
                return {...prev, date: 'Please enter a valid date and time!'}
            })
        }

        if(isValid){
            console.log("isValid");
            navigation.goBack();

            var message = `Notification has been scheduled for ${moment(date).format('DD/MM/YYYY | hh:mm A')}!`
            ToastAndroid.show(message, ToastAndroid.LONG);

            setTimeout(() => {
                ScheduleLocalNotification({
                    picture: picture_url,
                    date: scheduleDate,
                    title: form?.title,
                    message: form?.subtitle,
                    tag: currentTag
                })
                
                setButtonLoader(false)
            }, 2000);
        }else{
            setButtonLoader(false)
        }

    }


    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <View style={{width: screenWidth,  padding: 20}} >
                    <Input
                        label="Title"
                        placeholder="Enter the title"
                        error={errors.title}
                        onChangeText={(text) => {onChange({name: 'title', value: text,}); setErrors({}); }}
                    />

                    <Input
                        label="Sub Title"
                        placeholder="Enter the subtitle"
                        error={errors.subtitle}
                        onChangeText={(text) => {onChange({name: 'subtitle', value: text,}); setErrors({}); }}
                    />

                    <View>
                        <Text style={styles.label} >Select Tag</Text>

                        {tagOptions?.map((item, index) => (
                            <TouchableOpacity 
                                key={index}
                                onPress={() => {setCurrentTag(item.title)}}
                                style={[styles.tagContainer, {borderColor: item.title == currentTag ? colors.primary : colors.light_gray }]} >
                                <Icon type="AntDesign" name="calendar" size={30} color={colors.black} />
                                <Text style={[styles.dateText, {marginLeft: 12}]} >{item.title}</Text>
                            </TouchableOpacity>
                        ))}
                        
                    </View>



                    <View style={{marginTop: 10}} >
                        <Text style={styles.label} >Schedule Date</Text>
                        <TouchableOpacity 
                            onPress={() => {setShowscheduleDate(true)}}
                            style={styles.dateSelectionContaner} >
                            <Text style={styles.dateText} >{moment(scheduleDate).local().format('ddd, DD-MM-YYYY | hh:mm A')}</Text>
                            <Icon type="AntDesign" name="calendar" size={30} color={colors.black} />
                        </TouchableOpacity>

                        <DatePicker 
                            modal={true}
                            date={scheduleDate} 
                            open={showScheduleDate}
                            onDateChange={setScheduleDate} 
                            onConfirm={(value) => {
                                console.log("confim = > "); 
                                setErrors({}); 
                                setScheduleDate(value); 
                                setShowscheduleDate(false); 
                            }}
                            onCancel={() => {
                                setErrors({}); 
                                setShowscheduleDate(false);
                            }}
                        />
                    </View>
                    {errors.date && <Text style={styles.error} >{errors.date}</Text>}

                    </View>
                    
            </ScrollView>
            

            <PrimaryButton
                title="Trigger Notification"
                buttonLoader={buttonLoader}
                onPress={() => {
                    triggerNotification()
                }}
            />
        </SafeAreaView>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1, 
        //padding: 20,
        paddingBottom: 0,
        alignItems: 'center',
        justifyContent: 'space-between', 
        backgroundColor: colors.off_white,
    },
    title: {
        fontSize: 36,
        fontFamily: Poppins.Regular,
        color: colors.dark_gray,
    },
    label: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginBottom: 4,
    },
    dateContainer: {
        flex: 1,
        borderTopWidth: 8, 
        paddingHorizontal: 20,
        borderColor: colors.light_gray, 
        paddingVertical: 10, 
        marginBottom: 4,
    },
    tagContainer: {
        height: 60,
        borderWidth: 1,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 20,
        borderRadius: 10,
        marginBottom: 10,
    },
    dateText: {
        fontSize: 14, 
        color: colors.black, 
        fontFamily: Poppins.Medium,
    },
    dateSelectionContaner: {
        height: 60,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 20,
        borderRadius: 10,
        borderColor: colors.gray,
    },
    error: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.reddish,
        marginTop: 4,
    },
})

export default TriggerNotification