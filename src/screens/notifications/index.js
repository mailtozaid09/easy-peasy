import React, { useState, useEffect } from 'react';
import {Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, SafeAreaView, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { screenWidth } from '../../global/constants';

import PrimaryButton from '../../components/button/PrimaryButton';
import OutlineButton from '../../components/button/OutlineButton';

import LottieView from 'lottie-react-native';

import { LocalNotification } from '../../utils/LocalPushController';

const NotificationScreen = ({navigation}) => {

    const [buttonLoader, setButtonLoader] = useState(false);
    const notificationArray = [
        {
            picture: 'https://images.unsplash.com/photo-1553697388-94e804e2f0f6?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OTR8fHRyYXZlbHxlbnwwfHwwfHx8MA%3D%3D',
            title: "Psst! Heard your passport whispering - it needs more stamps!",
            message: "Time for new adventures, don't you think? 🌟🌎",
        },
        {
            picture: 'https://plus.unsplash.com/premium_photo-1675484743423-57da4e8011c2?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OXx8dHJhdmVsfGVufDB8fDB8fHww',
            title: "Don't let your luggage get lonely!",
            message: "It's begging for another trip. Where to next? 🛄✈️",
        },
        {
            picture: 'https://images.unsplash.com/photo-1488085061387-422e29b40080?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTR8fHRyYXZlbHxlbnwwfHwwfHx8MA%3D%3D',
            title: "Did you hear that? Oh, it's just the sound of a new adventure waiting for you!",
            message: "Time to discover the unknown with our app. 🗺️🔍",
        },
        {
            picture: 'https://images.unsplash.com/photo-1502301197179-65228ab57f78?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mjd8fHRyYXZlbHxlbnwwfHwwfHx8MA%3D%3D',
            title: "Just like the 'Titanic,' your wanderlust is unsinkable!",
            message: "Explore our app for destinations that won't leave you feeling like Jack. 🚢🌍",
        },
        {
            picture: 'https://images.unsplash.com/photo-1500835556837-99ac94a94552?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8dHJhdmVsfGVufDB8fDB8fHww',
            title: "Shhh... 'The Secret Garden' to unlock your wanderlust?",
            message: "Our app holds the key to discover hidden travel gems! 🗝️🌺",
        },
    ]

    const triggerNotification = () => {
        
        setButtonLoader(true)
        var arrIndex = Math.floor(Math.random() * (5 - 0 + 1)) + 0;

        setTimeout(() => {
            LocalNotification({
                picture: notificationArray[arrIndex]?.picture,
                title: notificationArray[arrIndex]?.title,
                message: notificationArray[arrIndex]?.message,
                tag: 'Today'
            })
            setButtonLoader(false)
        }, 2000);
        
       
    }

    return (
        <SafeAreaView style={styles.container}>
            <LottieView
                source={media.bell_lottie} 
                autoPlay 
                loop 
                style={{height: screenWidth*1.2, width: screenWidth*1.2,}}
            />

            <View>
                <OutlineButton
                    title="Trigger Notification"
                    buttonLoader={buttonLoader}
                    onPress={() => {
                        triggerNotification()
                    }}
                />
                <PrimaryButton
                    title="Trigger Custom Notification"
                    onPress={() => {
                        navigation.navigate('TriggerNotification')
                    }}
                />
            </View>
        </SafeAreaView>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1, 
        padding: 20,
        paddingBottom: 0,
        alignItems: 'center',
        justifyContent: 'space-between', 
        backgroundColor: colors.off_white,
    },
    title: {
        fontSize: 36,
        fontFamily: Poppins.Regular,
        color: colors.dark_gray,
    },
})

export default NotificationScreen