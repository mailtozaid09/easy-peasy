import React, { useState, useEffect } from 'react';
import {Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, SafeAreaView, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { screenWidth } from '../../global/constants';

import LottieView from 'lottie-react-native';

const HomeScreen = () => {
    return (
        <SafeAreaView style={styles.container}>
            <LottieView
                source={media.hi_lottie} 
                autoPlay 
                loop 
                style={{height: screenWidth-70, width: screenWidth-70, }}
            />
            <Text style={styles.title}>Welcome aboard the Easy-Peasy Travel App!</Text>
            <Text style={styles.subtitle}>Your ticket to hassle-free adventures awaits. Let's make every journey a breeze together!</Text>
        </SafeAreaView>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        padding: 20,
        backgroundColor: colors.white,
    },
    title: {
        fontSize: 20,
        marginTop: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 16,
        marginTop: 10,
        fontFamily: Poppins.Regular,
        color: colors.black,
        textAlign: 'center',
    },
})

export default HomeScreen