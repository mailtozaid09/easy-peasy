import React, { useRef } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Vibration, ToastAndroid, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'
import { LocalNotification, ScheduleLocalNotification } from '../../utils/LocalPushController'
import RemotePushController from '../../utils/RemotePushController'
import GetStartedSheet from '../../components/bottomsheet/GetStartedSheet'


const OnboardingScreen = ({navigation}) => {

    const refRBSheet = useRef();

    const enableVibration = () => {
        Vibration.vibrate(50);
    }

    const loginFunction = (val, isLogin) => {
        refRBSheet.current.close();

        switch(val){
            case 'email': 
            console.log("through >> email ");
            var message = 'Email Login is not implemented!';
            ToastAndroid.show(message, ToastAndroid.SHORT);
            break;

            case 'phone': 
            console.log("through >> phone ");
            navigation.navigate('Login', {params: {loginTypeEmail: false}})
            break;

            case 'google': 
            console.log("through >> google ");
            var message = 'Google Login is not implemented!';
            ToastAndroid.show(message, ToastAndroid.SHORT);
            break;

            case 'apple': 
            console.log("through >> apple ");
            var message = 'Apple Login is not implemented!';
            ToastAndroid.show(message, ToastAndroid.SHORT);

            default: 
            console.log("through >> default ");
        }
        
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imgContainer} >
                <Image source={media.getstarted} style={{height: 180, width: 180, resizeMode: 'contain', marginTop: 20}} />
                <Text style={styles.title} >Welcome to Easy-Peasy Travel App, where exploring the world is as easy as a tap!</Text>
                <Text style={styles.subtitle} > With our easy-peasy app, discovering new destinations and planning your dream getaway is just a tap away. Let's kickstart your adventure today!</Text>
            </View>
            <PrimaryButton
                title="Get Started" 
                onPress={() => {
                    refRBSheet.current.open();
                    enableVibration();
                }}
            />

            <GetStartedSheet
                refRBSheet={refRBSheet}
                enableVibration={() => {enableVibration()}}
                loginFunction={(val, isLogin) => loginFunction(val, isLogin)}
                closeSheet={() => {refRBSheet.current.close();}}
            />


            <RemotePushController />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        paddingBottom: 0,
        backgroundColor: colors.bg_color,
    },
    imgContainer: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    title: {
        fontSize: 18,
        paddingHorizontal: 15,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textAlign: 'center',
        marginTop: 20,
    },
    subtitle: {
        fontSize: 14,
        paddingHorizontal: 15,
        marginTop: 10,
        fontFamily: Poppins.Medium,
        color: colors.gray_color,
        textAlign: 'center'
    }
})

export default OnboardingScreen