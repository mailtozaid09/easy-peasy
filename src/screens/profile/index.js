import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'

import AlertModal from '../../components/modal/AlertModal'

import { useDispatch, useSelector } from 'react-redux'
import { userLoggedIn } from '../../store/modules/auth/actions'
import { LocalNotification } from '../../utils/LocalPushController'

import auth from '@react-native-firebase/auth';




const ProfileScreen = ({navigation}) => {

    const dispatch = useDispatch();
    
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');
    
    const user_details = useSelector(state => state.auth.user_details);

    const profile_options = [
        {
            title: 'Dashboard',
            icon: media.home_active,
        },
        {
            title: 'Wallet',
            icon: media.wallet_active,
        },
        {
            title: 'Guide',
            icon: media.guide_active,
        },
        {
            title: 'Chart',
            icon: media.chart_active,
        },
        {
            title: 'Logout',
            icon: media.logout,
        },
    ]

    useEffect(() => {
     
    }, [])
    

    const onProfileOptions = (option) => {

        if(option == 'Dashboard'){
            navigation.goBack()
        }else if(option == 'Wallet'){
            navigation.goBack()
            navigation.navigate('WalletStack')
        }else if(option == 'Guide'){
            navigation.goBack()
            navigation.navigate('Guide')
        }else if(option == 'Chart'){
            navigation.goBack()
            navigation.navigate('ChartStack')
        }else if(option == 'Logout'){
            setAlertType('Logout')
            setAlertTile("Are you sure!");
            setAlertDescription("You want to logout?");
            setShowAlertModal(true)
        }
        
    }


    const logoutFunction = () => {
        LocalNotification({
            title: "You've been logged out successfully.!",
            message: `Thank you for using Easy-Peasy!`,
        })
        
        console.log("successfully logged out");
 
        dispatch(userLoggedIn(false))
        
        auth().signOut()
        .then((resp) => {
            console.log("logging out == >> ",resp);
            navigation.navigate('LoginStack', {screen: 'Onboarding'})
            navigation.goBack()
        })
        .catch(error => {
            console.log("error logging out" + error);
        });
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <View style={styles.imageIconContainer}>
                    <Image source={media.person_active} style={{height: 35, width: 35, resizeMode: 'contain'}} />
                </View> 

                <View style={{alignItems: 'center'}} >
                    <Text style={styles.title} >Easy-Peasy Travel App</Text>
                    <Text style={styles.subtitle} >{user_details?.mobileNumber}</Text>
                </View>


                <View style={styles.profileOptions} >
                    {profile_options?.map((item, index) => (
                        <TouchableOpacity  
                            key={index}
                            activeOpacity={0.5} 
                            onPress={() => {onProfileOptions(item.title)}}
                            style={[styles.profileOptionsContainer, item.title == 'Logout' && {borderBottomWidth: 0}]} 
                        >
                            <View style={[styles.optionIconContainer, item.title == 'Logout' && {backgroundColor: colors.red,}]} >
                                <Image source={item.icon} style={{height: 26, width: 26, }} />
                            </View>
                            <Text style={styles.optionTitle} >{item.title}</Text>
                        </TouchableOpacity>
                    ))}
                </View>

                
            </View>
            {showAlertModal && 
                    <AlertModal
                        alertType={alertType}
                        title={alertTile}
                        description={alertDescription}
                        onLogout={() => {
                            setShowAlertModal(false); 
                            logoutFunction();
                        }}
                        closeModal={() => {setShowAlertModal(false); }} 
                    />
                }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    mainContainer: {
        flex: 1, 
        width: '100%',
        alignItems: 'center', 
    },
    title: {
        fontSize: 18, 
        marginTop: 10,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    subtitle: {
        fontSize: 16, 
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginBottom: 20,
    },
    imageIconContainer: {
        height: 70,
        width: 70,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: colors.primary,
        backgroundColor: colors.primary,
    },
    optionTitle: {
        color: colors.black,
        fontSize: 16,
        fontFamily: Poppins.Medium
    },
    profileOptions: {
        width: '100%',
        borderRadius: 10,
        marginTop: 0,
        padding: 15,
        paddingVertical: 0,
        backgroundColor: colors.off_white,
    },
    profileOptionsContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderColor: colors.primary,
    },
    optionIconContainer: {
        height: 40,
        width: 40,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        backgroundColor: colors.white,
    }
})

export default ProfileScreen