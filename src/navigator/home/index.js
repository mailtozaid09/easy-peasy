import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import ProfileScreen from '../../screens/profile';
import HomeScreen from '../../screens/home';
import HomeHeader from '../../components/header/HomeHeader';
import NotificationScreen from '../../screens/notifications';
import TriggerNotification from '../../screens/notifications/TriggerNotification';

const Stack = createStackNavigator();

const HomeStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    header: () => (<HomeHeader />),
                    headerTitle: 'Home',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Profile',
                    headerTitleAlign: 'center',
                    headerTintColor: colors.black,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            
            <Stack.Screen
                name="Notification"
                component={NotificationScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Notifications',
                    headerTitleAlign: 'center',
                    headerTintColor: colors.black,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="TriggerNotification"
                component={TriggerNotification}
                options={{
                    headerShown: true,
                    headerTitle: 'Trigger Notification',
                    headerTitleAlign: 'center',
                    headerTintColor: colors.black,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 20, 
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    headerStyle: {
        backgroundColor: colors.white,
    }
})

export default HomeStack