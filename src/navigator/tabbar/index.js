import * as React from 'react';
import {Image, Platform, StyleSheet, Text, View} from 'react-native';
import {NavigationContainer, getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { screenWidth } from '../../global/constants';

import moment from 'moment';

import TodayScreen from '../../screens/today';
import TomorrowScreen from '../../screens/tomorrow';
import YesterdayScreen from '../../screens/yesterday';

import HomeStack from '../home';
import WalletStack from '../wallet';
import ChartStack from '../chart';



const TopTab = createMaterialTopTabNavigator();
const BottomTab = createBottomTabNavigator();

const TopTabNavigator = () => {

    var today_date = new Date()
    var yesterday_date = moment(today_date).subtract(1, "days");
    var tomorrow_date = moment(today_date).subtract(-1, "days");
    
    return (
        <TopTab.Navigator 
            initialRouteName='Today' 
            screenOptions={({ route }) => ({
                tabBarPressColor: '#00000010',
                tabBarStyle: styles.tabBarStyle,
                tabBarItemStyle: styles.tabBarItemStyle,
                tabBarIndicatorStyle: styles.tabBarIndicatorStyle,

                tabBarIcon: ({ focused, color, size }) => {

                let date, routeName = 'home', format = 'DD MMM';

                if (route.name === 'Yesterday') {
                    routeName = 'Yesterday' 
                    date = moment(yesterday_date).format(format)
                } else if (route.name === 'Today') {
                    routeName = 'Today' 
                    date = moment(today_date).format(format)
                } else if (route.name === 'Tomorrow') {
                    routeName = 'Tomorrow' 
                    date = moment(tomorrow_date).format(format)
                }
                return(
                    <View style={styles.topbarContainer} >
                        <Text style={styles.topbarTitle} >{routeName}</Text>
                        <Text style={styles.topbarSubTitle} >{date}</Text>
                    </View>
                );
                },
            })}
        >
            <TopTab.Screen 
                name="Yesterday" 
                component={YesterdayScreen} 
                options={({navigation, route}) => ({
                    tabBarShowLabel: false,
                })}
            />
            <TopTab.Screen 
                name="Today" 
                component={TodayScreen} 
                options={({navigation, route}) => ({
                    tabBarShowLabel: false,
                })}
            />
            <TopTab.Screen 
                name="Tomorrow" 
                component={TomorrowScreen} 
                options={({navigation, route}) => ({
                    tabBarShowLabel: false,
                })}
            />
        </TopTab.Navigator>
    );
};

const AppNavigator = () => {
    return (
        <>
            <BottomTab.Navigator 
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                    let iconName, routeName, height = 30, width = 30 ;
        
                    if (route.name === 'HomeStack') {
                        iconName = focused ? media.home_active : media.home
                        routeName = 'Home' 
                    } 

                    else if (route.name === 'WalletStack') {
                        iconName = focused ? media.wallet_active : media.wallet
                        routeName = 'Wallet' 
                      
                    }
                    else if (route.name === 'Guide') {
                        iconName = focused ? media.guide_active : media.guide
                        routeName = 'Guide' 
                    }
                    else if (route.name === 'ChartStack') {
                        iconName = focused ? media.chart_active : media.chart
                        routeName = 'Chart'
                    }
                    return(
                        <View style={styles.tabbarIconContainer} >
                            <Image source={iconName} style={{height: height, width: width, resizeMode: 'contain'}}/>
                            <Text style={focused ? styles.tabbarTitleFocused : styles.tabbarTitle}>{routeName}</Text>
                        </View>
                    );
                    },
                })}
                >
                <BottomTab.Screen 
                    name="HomeStack" 
                    component={HomeStack} 
                    options={({navigation, route}) => ({
                        headerShown: false,
                        tabBarShowLabel: false,
                        tabBarStyle: ((route) => {
                            const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                                if (routeName === 'Profile' || routeName === 'Notification' || routeName === 'TriggerNotification') {
                                    return { display: "none",  }
                                }
                            return styles.tabbarStyle
                        })(route)
                    })}
                />
                <BottomTab.Screen 
                    name="WalletStack" 
                    component={WalletStack} 
                    options={({navigation, route}) => ({
                        headerShown: false,
                        tabBarShowLabel: false,
                        tabBarStyle: ((route) => {
                            const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                                if (routeName === 'Home') {
                                    return { display: "none",  }
                                }
                            return styles.tabbarStyle
                        })(route)
                    })}
                />
                <TopTab.Screen 
                    name="Guide" 
                    component={TopTabNavigator} 
                    options={({navigation, route}) => ({
                        headerShown: true,
                        headerTitle: 'Itenary Form',
                        headerTitleAlign: 'center',
                        headerTintColor: colors.white,
                        headerStyle: styles.headerStyle,
                        headerTitleStyle: styles.headerTitleStyle,
                        tabBarShowLabel: false,
                        tabBarStyle: ((route) => {
                            const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                                if (routeName === 'Home') {
                                    return { display: "none",  }
                                }
                            return styles.tabbarStyle
                        })(route)
                    })}
                />
                <BottomTab.Screen 
                    name="ChartStack" 
                    component={ChartStack} 
                    options={({navigation, route}) => ({
                        headerShown: false,
                        tabBarShowLabel: false,
                        tabBarStyle: ((route) => {
                            const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                                if (routeName === 'Home') {
                                    return { display: "none",  }
                                }
                            return styles.tabbarStyle
                        })(route)
                    })}
                />
            </BottomTab.Navigator>
        </>
    );
};


const styles = StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 80,
        paddingTop: Platform.OS == 'ios' ? 15 : 10, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        backgroundColor: colors.white,
        borderTopLeftRadius: 24,
        borderTopRightRadius: 24,
    },

    tabbarTitle: {
        fontSize: 14, 
        fontFamily: Poppins.Regular,
        color: colors.dark_gray,
        marginTop: 2,
    },
    tabbarTitleFocused: {
        fontSize: 14, 
        fontFamily: Poppins.Regular,
        color: colors.primary,
        marginTop: 2,
    },
    tabbarIconContainer: {
        alignItems: 'center', 
        justifyContent: 'center',
    },
    topbarTitle: {
        fontSize: 16, 
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    topbarSubTitle: {
        fontSize: 14, 
        fontFamily: Poppins.Regular,
        color: colors.gray,
    },
    topbarContainer: {
        height: 55, 
        width: screenWidth/3-10, 
        alignItems: 'center', 
        justifyContent: 'center',
    },


    tabBarStyle: {
        height: 75,
        backgroundColor: colors.white,
    },
    tabBarItemStyle: {
        left: -42, 
        width: screenWidth/3, 
        paddingBottom: 40,
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    tabBarIndicatorStyle: { 
        height: 3, 
        width: 76, 
        left: 24,
        borderRadius: 2,
        backgroundColor: colors.primary, 
    },


    headerTitleStyle: {
        fontSize: 20, 
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    headerStyle: {
        backgroundColor: colors.white,
    }
})
export default AppNavigator