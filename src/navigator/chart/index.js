import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import ChartScreen from '../../screens/chart';

const Stack = createStackNavigator();

const ChartStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Chart" 
        >
            <Stack.Screen
                name="Chart"
                component={ChartScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Chart',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 20, 
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    headerStyle: {
        backgroundColor: colors.white,
    }
})

export default ChartStack