import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';


import { useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';


import Tabbar from './tabbar';
import LoginStack from './login';

import messaging from '@react-native-firebase/messaging';
import PushNotification, {Importance} from "react-native-push-notification";

const Stack = createStackNavigator();


const Navigator = ({}) => {

    const navigation = useNavigation()

    const user_logged_in = useSelector(state => state.auth.user_logged_in);

    const user_details = useSelector(state => state.auth.user_details);

    console.log("user_details ->> ", user_details);
    console.log("user_logged_in ->> ", user_logged_in);

    useEffect(() => {
       
    }, [])


    const getFCMToken = async () => {
        try {
          const token = await messaging().getToken();
          console.log("token > > >> > ",token);
        } catch (e) {
          console.log(error);
        }
    };
    
    useEffect(() => {
        getFCMToken();
    
        messaging().onMessage(async remoteMessage => {
            console.log('A new FCM message arrived!');
        });
    
        messaging().onNotificationOpenedApp(remoteMessage => {
          
            if (remoteMessage) {
                console.log('onNotificationOpenedApp');
                checkForNotification(remoteMessage, 'onNotificationOpenedApp')
            }
        });
    
        messaging()
          .getInitialNotification()
          .then(remoteMessage => {
            if (remoteMessage) {
                console.log('getInitialNotification');
                checkForNotification(remoteMessage, 'getInitialNotification')
            }
          });
        messaging().setBackgroundMessageHandler(async remoteMessage => {
            console.log('Message handled in the background!');
        });
    }, []);


    const checkForNotification = (message, from) => {
    
        switch(message.data?.day){
            case 'Yesterday':
            console.log("go to >> Yesterday ");
            navigation.navigate('Guide', {screen: 'Yesterday'})
            
            break;

            case 'Today':
            console.log("go to >> Today ");
            navigation.navigate('Guide', {screen: 'Today'})
            
            break;

            case 'Tomorrow':
            console.log("go to >> Tomorrow ");
            navigation.navigate('Guide', {screen: 'Tomorrow'})
            
            break;

            default:
            console.log("through >> default ");
            navigation.navigate('Home');
        }
    }



    PushNotification.configure({
        onRegister: function (token) {
           console.log("TOKEN:", token);
         },
       
         onNotification: function (notification) {
           console.log("LOCAL !!!  NOTIFICATION:", notification);
       
           checkNotification(notification)
       
         },
        onAction: function (notification) {
           console.log("ACTION:", notification.action);
           console.log("NOTIFICATION:", notification);
         },
       
         onRegistrationError: function(err) {
           console.error(err.message, err);
         },
       
         popInitialNotification: true,
         requestPermissions: true,
    });
       
       
       
    const checkNotification = (notification) => {

        switch(notification?.tag){
            case 'Yesterday':
            console.log("go to >> Yesterday ");
            navigation.navigate('Guide', {screen: 'Yesterday'})
            
            break;

            case 'Today':
            console.log("go to >> Today ");
            navigation.navigate('Guide', {screen: 'Today'})
            
            break;

            case 'Tomorrow':
            console.log("go to >> Tomorrow ");
            navigation.navigate('Guide', {screen: 'Tomorrow'})
            
            break;

            default:
            console.log("through >> default ");
            navigation.navigate('Home');
        }
    }

    return (
        <>
        <Stack.Navigator 
            initialRouteName={user_logged_in ? 'Tabbar' : 'LoginStack'}  
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
        
        </>
    );
}

export default Navigator