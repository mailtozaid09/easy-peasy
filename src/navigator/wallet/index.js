import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import WalletScreen from '../../screens/wallet';

const Stack = createStackNavigator();

const WalletStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Wallet" 
        >
            <Stack.Screen
                name="Wallet"
                component={WalletScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Wallet',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 20, 
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    headerStyle: {
        backgroundColor: colors.white,
    }
})

export default WalletStack