import { SET_USER_DETAILS, USER_LOGGED_IN } from "./actionTypes";

const INITIAL_STATE = {
    user_logged_in: false,
    user_details: null,
};

export default function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
      
    case SET_USER_DETAILS: {
        const {
            mobileNumber,
        } = action.payload
      
        return {
            ...state,
            user_details: {
                mobileNumber,
            }
        };
    }

    case USER_LOGGED_IN: {
        return {
            ...state,
            user_logged_in: action.payload.user_logged_in,
        }
    };
    
    default:
        return state;
    }
}
