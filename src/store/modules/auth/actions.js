import { 
    SET_USER_DETAILS, USER_LOGGED_IN,
} from './actionTypes';

export function setUserDetails(params) {
    return {
        type: SET_USER_DETAILS,
        payload: {
            mobileNumber: params.mobileNumber,
        },
    };
};


export function userLoggedIn(user_logged_in) {
	return {
		type: USER_LOGGED_IN,
		payload: {
			user_logged_in,
		},
	};
}
