import { useNavigation } from "@react-navigation/native";
import PushNotification, {Importance} from "react-native-push-notification";


// PushNotification.configure({
//  onRegister: function (token) {
//     console.log("TOKEN:", token);
//   },

//   onNotification: function (notification) {
//     console.log("LOCAL !!!  NOTIFICATION:", notification);
   
//     // console.log("notification=> ttitle >> ", notification?.title);
//     // console.log("notification=> message >> ", notification?.message);
//     // console.log("notification=> tag >> ", notification?.tag);
//     //checkForNotification(notification)

//   },
//  onAction: function (notification) {
//     console.log("ACTION:", notification.action);
//     console.log("NOTIFICATION:", notification);
//   },

//   onRegistrationError: function(err) {
//     console.error(err.message, err);
//   },



//   popInitialNotification: true,
//   requestPermissions: true,
// });

PushNotification.createChannel(
    {
        channelId: "channel-id", 
        channelName: "My channel", 
        channelDescription: "A channel to categorise your notifications",
        playSound: true, 
        soundName: "default", 
        importance: Importance.HIGH,
        vibrate: true,
    },
    (created) => console.log(`createChannel returned '${created}'`) 
);

export const LocalNotification = (props) => {
    const { title, message, description, picture, tag} = props;

    PushNotification.localNotification({
        channelId: "channel-id", 
        channelName: "My channel", 
        autoCancel: true,
        playSound: true, 
        soundName: "default", 
        importance: Importance.HIGH,
        vibrate: true,
        bigText: description,
        title: title,
        message: message,
        picture: picture,
        color: 'red',
        tag: tag,
    })
}


export const ScheduleLocalNotification = (props) => {

  const { title, message, description, date, picture, tag} = props;

  PushNotification.localNotificationSchedule({
      channelId: "channel-id", 
      channelName: "My channel", 
      autoCancel: true,
      playSound: true, 
      soundName: "default", 
      importance: Importance.HIGH,
      vibrate: true,
      date: date,
      bigText: description,
      title: title,
      message: message,
      picture: picture,
      tag: tag,
  })
}